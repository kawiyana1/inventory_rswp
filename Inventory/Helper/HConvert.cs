﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Inventory.Helper
{
    public static class HConvert
    {
        public static string Error(Exception ex)
        {
            if (ex.InnerException != null) ex = ex.InnerException;
            if (ex.InnerException != null) ex = ex.InnerException;
            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = ex.Message });
        }

        public static string Error(string ex)
        {
            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = ex });
        }

        public static string Success()
        {
            return JsonConvert.SerializeObject(new { IsSuccess = true });
        }

        public static string ExToString(this DbEntityValidationException e)
        {
            string error = "";
            foreach (var eve in e.EntityValidationErrors)
            {
                error = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors)
                    error += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
            }
            throw new Exception("User Activity : " + error);
        }

        public static decimal? ToDecimal(this string value, bool default0 = true)
        {
            if (value == null && default0 == false) return null;
            if (value == null) return 0;
            var newValue = value.Replace("Rp. ", "").Replace(",", "").Replace(".", ",");
            if (newValue == "-") return 0;
            return decimal.Parse(newValue == "" ? "0" : newValue);
        }

        public static decimal ToDecimal(this string value)
        {
            if (value == null) return 0;

            // US
            //var newValue = value.Replace("Rp. ", "").Replace(",", "");
            // ID
            var newValue = value.Replace("Rp. ", "").Replace(",", "").Replace(".", ",");

            if (newValue == "-") return 0;
            return decimal.Parse(newValue == "" ? "0" : newValue);
        }

        public static string ToMoney(this decimal value)
        {
            var value_string = value.ToString();

            // US
            //var coma_split = value_string.Split('.');
            // ID
            var coma_split = value_string.Split(',');

            int coma_length = 0;
            if (coma_split.Length > 1)
                coma_length = coma_split[1].Replace("0", "").Length;
            value_string = Math.Round(value, coma_length).ToString();

            decimal saveComa = 0;
            if (coma_split.Length > 1)
            {
                value_string = coma_split[0];
                saveComa = decimal.Parse("0," + coma_split[1]);
            }
            var newValue = "";
            var index = 1;
            for (var j = value_string.Length - 1; j >= 0; j--)
            {
                newValue = value_string[j] + newValue;
                if (index % 3 == 0 && j != 0)
                    newValue = "," + newValue;
                index++;
            }
            if (saveComa > 0) newValue += "." + (saveComa.ToString().Split(',')[1]);
            return newValue == "" ? "0" : newValue;
        }

        public static string ToRoman(this int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public static string Bulan(this DateTime tgl)
        {
            if (tgl.Month == 1) return "Januari";
            else if (tgl.Month == 2) return "Februari";
            else if (tgl.Month == 3) return "Maret";
            else if (tgl.Month == 4) return "April";
            else if (tgl.Month == 5) return "Mei";
            else if (tgl.Month == 6) return "Juni";
            else if (tgl.Month == 7) return "Juli";
            else if (tgl.Month == 8) return "Agustus";
            else if (tgl.Month == 9) return "September";
            else if (tgl.Month == 10) return "Oktober";
            else if (tgl.Month == 11) return "November";
            else if (tgl.Month == 12) return "Desember";
            throw new ArgumentOutOfRangeException("something bad happened");
        }
    }
}