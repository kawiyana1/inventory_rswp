//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_GetmBarangHeader
    {
        public int Barang_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Kelompok { get; set; }
        public string KelompokJenis { get; set; }
        public Nullable<short> Kategori_Id { get; set; }
        public string Nama_Kategori { get; set; }
        public Nullable<int> SubKategori_Id { get; set; }
        public string Nama_Sub_Kategori { get; set; }
        public Nullable<int> Kelas_ID { get; set; }
        public string Nama_Kelas { get; set; }
        public Nullable<short> GolonganID { get; set; }
        public string NamaGolongan { get; set; }
        public string KelompokGrading { get; set; }
        public Nullable<bool> FormulariumUmum { get; set; }
        public Nullable<bool> FormulariumJKN { get; set; }
        public Nullable<short> Beli_Satuan_Id { get; set; }
        public string Beli_Satuan_Nama { get; set; }
        public Nullable<decimal> Harga_Beli { get; set; }
        public Nullable<double> PPn_Persen { get; set; }
        public Nullable<short> Stok_Satuan_ID { get; set; }
        public string Stok_Satuan_Nama { get; set; }
        public Nullable<decimal> Harga_Pokok { get; set; }
        public Nullable<double> Konversi { get; set; }
        public Nullable<decimal> HRataRata { get; set; }
        public Nullable<double> CNOnFaktur { get; set; }
        public Nullable<double> CNOffFaktur { get; set; }
        public Nullable<int> Supplier_ID { get; set; }
        public string Nama_Supplier { get; set; }
        public bool Aktif { get; set; }
        public Nullable<bool> UseHET { get; set; }
        public Nullable<decimal> NilaiHET { get; set; }
    }
}
