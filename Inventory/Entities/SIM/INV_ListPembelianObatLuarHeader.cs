//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_ListPembelianObatLuarHeader
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string Kode_Supplier { get; set; }
        public string Type_Pembayaran { get; set; }
        public string Alasan { get; set; }
        public string SectionID { get; set; }
        public Nullable<short> UserID { get; set; }
        public Nullable<bool> Batal { get; set; }
        public string DokterID { get; set; }
        public string Vendor { get; set; }
        public string SectionName { get; set; }
        public string Dokter { get; set; }
    }
}
