//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventory.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class INV_ListMataUang
    {
        public byte Currency_ID { get; set; }
        public string Currency_Code { get; set; }
        public string Currency_Name { get; set; }
        public string Keterangan { get; set; }
        public bool Currency_default { get; set; }
    }
}
