﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Inventory.Models.Purchasing;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PurchasingOrderController : Controller
    {
        private string tipepelayanan = "PENGADAAN";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            var status = filter.FirstOrDefault(x => x.Key == "Realisasi").Value == "Realisasi" ? "Realisasi" : "Open";
            if (status == "Realisasi")
                return _listrealisasi(orderby, orderbytype, pagesize, pageindex, filter);
            else
                return _listopen(orderby, orderbytype, pagesize, pageindex, filter);
        }

        public string _listopen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_listPengadaaanOrder.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.No_Permintaan.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Permintaan >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Permintaan <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Permintaan.ToString("dd/MM/yyyy"),
                        No = m.No_Permintaan,
                        TanggalDIbutuhkan = m.Tgl_Dibutuhkan == null ? "" : m.Tgl_Dibutuhkan.Value.ToString("yyyy-MM-dd"),
                        Supplier = m.Nama_Supplier,
                        Realisasi = false
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string _listrealisasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                orderby = orderby == "No_Permintaan" ? "No_Order" :
                    orderby == "Tgl_Permintaan" ? "Tgl_Order" :
                    orderby == "Nama_Supplier" ? "Supplier_Nama_Supplier" :
                    "";

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_listPengadaaanOrderRealisasi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Supplier_Nama_Supplier.Contains(x.Value) ||
                                y.No_Order.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Order.ToString("dd/MM/yyyy"),
                        No = m.No_Order,
                        Supplier = m.Supplier_Nama_Supplier,
                        Batal = m.Status == "Batal",
                        Terima = m.Status != "Belum Terima", // Terima Semua || Terima Sebagian,
                        Status = m.Status,
                        Realisasi = true
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PurchasingOrderViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<PurchasingDetailOrderViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            if (model.Supplier == 0) throw new Exception("Supplier tidak boleh kosong");
                            var _id = s.INV_inserOrder(
                                model.Tanggal,
                                model.Keterangan,
                                model.Supplier,
                                model.Total,
                                model.NoPermintaan,
                                model.JenisPengadaan
                            ).FirstOrDefault();

                            if (_id == null) throw new Exception("INV_inserOrder tidak mendapatkan nobukti");
                            id = _id.OrderID ?? 0;

                            foreach (var x in model.Detail)
                            {
                                s.INV_inserOrderDetail(
                                    x.Qty,
                                    x.Harga,
                                    model.NoPermintaan,
                                    (int)_id.OrderID,
                                    x.Id
                                );
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string DetailOrder(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListPermintaanPembelian.FirstOrDefault(x => x.No_Permintaan == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListPermintaanPembelianDetail.Where(x => x.Permintaan_ID == m.Permintaan_ID).OrderBy(x => x.Nama_Barang).ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoPermintaan = m.No_Permintaan,
                            Jenis = m.JenisPengadaanID,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Qty = x.Qty,
                            Harga = x.Harga_Beli,
                            Reference = m.No_Permintaan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListOrderHeader.FirstOrDefault(x => x.No_Order == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListOrderDetail.Where(x => x.Order_ID == m.Order_ID).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            SupplierId = m.Supplier_ID,
                            NoBukti = m.No_Order,
                            Tanggal = m.Tgl_Order.ToString("yyyy-MM-dd"),
                            SupplierNama = m.Nama_Supplier,
                            TanggalKirim = m.Tgl_Kirim == null ? "" : m.Tgl_Kirim.Value.ToString("yyyy-MM-dd"),
                            Keterangan = m.Keterangan,
                            NoPermintaan = m.No_Permintaan,
                            Total = m.Total_Nilai,
                            Id = m.Order_ID
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Qty = x.Qty_Order,
                            Harga = x.Harga_Order,
                            Reference = x.No_Permintaan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}