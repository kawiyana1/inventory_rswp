﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    public class Select2Controller : Controller
    {
        [HttpPost]
        public string Supplier(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<INV_GetSupplier> proses = s.INV_GetSupplier;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Supplier.Contains(filter) ||
                            x.Nama_Supplier.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Supplier).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Supplier_ID,
                        Kode = m.Kode_Supplier,
                        Nama = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Kode_Supplier(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<INV_GetSupplier> proses = s.INV_GetSupplier;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Supplier.Contains(filter) ||
                            x.Nama_Supplier.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Supplier).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kode_Supplier,
                        Kode = m.Kode_Supplier,
                        Nama = m.Nama_Supplier
                    });
                    //r.Add();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Supplier_Permintaan(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var lokasilogin = "";
                    if (Request.Cookies["Inventory_Section_Id"].Value == "SEC081")
                    {
                        lokasilogin = "V-007";
                    }
                    else if (Request.Cookies["Inventory_Section_Id"].Value == "SEC107")
                    {
                        lokasilogin = "V-006";
                    }
                    else
                    {
                        lokasilogin = "";
                    }

                    if (filter == null) {
                        filter = " ";
                    }
                    IQueryable<INV_ListSupplier> proses = s.INV_ListSupplier;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                        x.Kode_Kategori.Contains(filter) ||
                        x.Nama_Supplier.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Supplier).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Supplier_ID,
                        Kode = m.Kode_Supplier,
                        Nama = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Kategori(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<mKategori_Vendor> proses = s.mKategori_Vendor;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Kategori.Contains(filter) ||
                            x.Kategori_Name.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kode_Kategori,
                        Kode = m.Kode_Kategori,
                        Nama = m.Kategori_Name
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        [HttpPost]
        public string KategoriBarang(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<INV_mKategori> proses = s.INV_mKategori;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Kategori.Contains(filter) ||
                            x.Nama_Kategori.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kode_Kategori,
                        Kode = m.Kode_Kategori,
                        Nama = m.Nama_Kategori
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string SetupSubKategoriBarangKategori(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mKategori.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Kategori.Contains(filter) ||
                            x.Nama_Kategori.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kategori_ID,
                        Kode = m.Kode_Kategori,
                        Nama = m.Nama_Kategori
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string SetupSubKategoriStokOpname(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mSubKategori.AsQueryable();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Nama_Sub_Kategori.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Nama_Sub_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SubKategori_ID,
                        Kode = m.SubKategori_ID,
                        Nama = m.Nama_Sub_Kategori
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string SetupKelasBarangSubKategori(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mSubKategori.AsQueryable();
                    //var proses = s.INV_mSubKategori.Where(x => x.Kategori_ID == kategori).AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Sub_Kategori.Contains(filter) ||
                            x.Nama_Sub_Kategori.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Sub_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SubKategori_ID,
                        Kode = m.Kode_Sub_Kategori,
                        Nama = m.Nama_Sub_Kategori
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string SetupBarangKelas(int pagesize, int pageindex, string filter, int subkategori)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mKelas.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Kelas.Contains(filter) ||
                            x.Nama_Kelas.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Kelas).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kelas_ID,
                        Kode = m.Kode_Kelas,
                        Nama = m.Nama_Kelas
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string SetupBarangJenis(int pagesize, int pageindex, string filter, string kelompok)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mJenis.Where(x => x.Kelompok == kelompok).AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.KelompokJenis.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.KelompokJenis).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.KelompokJenis,
                        Nama = m.KelompokJenis
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Barang(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<Vw_Barang> proses = s.Vw_Barang;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Nama_Barang.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Barang).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string FarmasiInformasiHargaObatLookUpBarang(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                    var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                    var proses = s.INV_LookupBarang.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Barang.Contains(filter) ||
                            x.Nama_Barang.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Barang).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string NoReg(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<FAR_GetRegistrasi> proses = s.FAR_GetRegistrasi;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.NoReg.Contains(filter) ||
                            x.NRM.Contains(filter) ||
                            x.NamaPasien.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.NoReg).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.NoReg,
                        Kode = m.NoReg,
                        Nama = m.NRM + " - " + m.NamaPasien
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string NoRegRpt(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<RPT_FAR_GetRegistrasi> proses = s.RPT_FAR_GetRegistrasi;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.NoReg.Contains(filter) ||
                            x.NRM.Contains(filter) ||
                            x.NamaPasien.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderByDescending(x => x.TglReg).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.NoReg,
                        Kode = m.NoReg,
                        Nama = m.NRM + " - " + m.NamaPasien 
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Tipe(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<FAR_ListJenisKerjasama> proses = s.FAR_ListJenisKerjasama;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.JenisKerjasamaID.ToString().Contains(filter) ||
                            x.JenisKerjasama.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.JenisKerjasamaID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.JenisKerjasamaID,
                        Kode = m.JenisKerjasamaID,
                        Nama = m.JenisKerjasama
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Dokter(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<FAR_GetDokter> proses = s.FAR_GetDokter;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.DokterID.ToString().Contains(filter) ||
                            x.NamaDOkter.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.DokterID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.DokterID,
                        Kode = m.DokterID,
                        Nama = m.NamaDOkter
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string LokasiID(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Lokasi_ID.ToString().Contains(filter) ||
                            x.SectionID.ToString().Contains(filter) ||
                            x.SectionName.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Lokasi_ID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Lokasi_ID,
                        Kode = m.Lokasi_ID,
                        Nama = m.SectionName
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string SectionID(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.SectionID.ToString().Contains(filter) ||
                            x.SectionID.ToString().Contains(filter) ||
                            x.SectionName.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Lokasi_ID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SectionID,
                        Kode = m.SectionID,
                        Nama = m.SectionName
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string NoOrder(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    IQueryable<BL_trOrder> proses = s.BL_trOrder;
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Order_ID.ToString().Contains(filter) ||
                            x.No_Order.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.No_Order).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.No_Order,
                        Kode = m.Order_ID,
                        Nama = m.No_Order
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #region ===== O T H E R

        [HttpPost]
        public string Other_Section_Customer(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetCustomerSection.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Alamat_1.Contains(filter) ||
                            x.Nama_Customer.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Customer).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Customer_ID,
                        Kode = m.Kode_Customer,
                        Nama = m.Nama_Customer
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        [HttpPost]
        public string Other_Section_AkunMutasi(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetRekKategoriVendor.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Akun_No.Contains(filter) ||
                            x.Akun_Name.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.Akun_ID,
                        Nama = m.Akun_Name
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        #endregion

        [HttpPost]
        public string Vendor_Vendor_Supplier(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetKategoriVendor.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Kategori.Contains(filter) ||
                            x.Kategori_Name.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Kategori).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kode_Kategori,
                        Kode = m.Kode_Kategori,
                        Nama = m.Kategori_Name
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        public string Vendor_Vendor_Spesialis(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetSpesialisVendor.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.SpesialisID.Contains(filter) ||
                            x.SpesialisName.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.SpesialisID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SpesialisID,
                        Kode = m.SpesialisID,
                        Nama = m.SpesialisName
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        public string Vendor_Vendor_Spesialis_List(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetSpesialisVendor.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.SpesialisID.Contains(filter) ||
                            x.SpesialisName.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.SpesialisID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SpesialisName,
                        Kode = m.SpesialisID,
                        Nama = m.SpesialisName
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        public string Vendor_Vendor_SubSpesialis(int pagesize, int pageindex, string filter, string spesialisid)
        {
            try
            {

                using (var s = new SIMEntities())
                {
                    //var spesialis = s.ADM_GetSpesialisVendor.FirstOrDefault(x => x.SpesialisID == spesialisid);
                    var proses = s.ADM_GetSubSpesialisVendor.Where(x => x.SpesialisID == spesialisid).AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.SubSpesialisID.Contains(filter) ||
                            x.SubSpesialisName.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.SubSpesialisID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.SubSpesialisID,
                        Kode = m.SubSpesialisID,
                        Nama = m.SubSpesialisName
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        public string Vendor_Vendor_KodeKelompok(int pagesize, int pageindex, string filter, string keteranganSO)
        {
            try
            {

                using (var s = new SIMEntities())
                {
                    if(keteranganSO == "") throw new Exception("Keterangan tidak boleh kosong");
                    var proses = s.INV_ListKelompokJenis.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.KodeAwal.Contains(filter) ||
                            x.KelompokJenis.Contains(filter) ||
                            x.Kelompok.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.KelompokJenis).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.GroupBy(x => new { sId = x.KodeAwal, GroupId = x.KodeAwal }).Select(g => new {
                                    Id = g.Key.sId,
                                    Kode = g.Key.GroupId,
                                    Nama = g.Select(x => new { x.KodeAwal }) 
                    });
                    //var r = models.ConvertAll(m => new
                    //{
                    //    Id = m.KodeAwal,
                    //    Kode = m.KodeAwal,
                    //    Nama = m.KodeAwal
                    //});
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        public string Vendor_Vendor_JenisKelompok(int pagesize, int pageindex, string filter, string kodeawal)
        {
            try
            {

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListKelompokJenis.Where(x => x.KodeAwal == kodeawal).AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.KodeAwal.Contains(filter) ||
                            x.KelompokJenis.Contains(filter) ||
                            x.Kelompok.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.KelompokJenis).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.KelompokJenis,
                        Kode = m.KelompokJenis,
                        Nama = m.KelompokJenis
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }

        [HttpPost]
        public string SetupReportKelompokJenis(int pagesize, int pageindex, string filter, string kelompok)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_mJenis.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.KelompokJenis.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.KelompokJenis).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.KelompokJenis,
                        Nama = m.KelompokJenis
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string SetupReportJenisKerjasama(int pagesize, int pageindex, string filter, string kelompok)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.SIMmJenisKerjasama.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.JenisKerjasama.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.JenisKerjasamaID).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.JenisKerjasamaID,
                        Nama = m.JenisKerjasama
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
    }
}