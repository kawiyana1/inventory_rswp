﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PeminjamanReturController : Controller
    {
        private string tipepelayanan = "GUDANG";
        private string tipepelayanan2 = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan &&
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_List_BL_trPeminjamanRetur.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Peminjaman.Contains(x.Value) ||
                                y.No_Retur.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value));
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Retur >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Retur <= d);
                        }
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoRetur = m.No_Retur,
                        NoPeminjaman = m.No_Peminjaman,
                        Tgl_Retur = m.Tgl_Retur.Value.ToString("dd/MM/yyyy"),
                        Keterangan = m.Keterangan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PeminjamannReturViewModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
                            if (string.IsNullOrEmpty(model.NoPeminjaman)) throw new Exception("No Peminjaman Harus diisi");
                            if (model.Detail == null) model.Detail = new List<PeminjamanReturDetailViewModel>();

                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            foreach (var x in model.Detail)
                            {
                                if (x.Qty <= 0) throw new Exception("Qty tidak boleh kurang dari sama dengan 0");
                            }
                            id = s.INV_Insert_BL_trPeminjamanRetur(
                                model.Tanggal,
                                model.NoPeminjaman,
                                model.Keterangan,
                                model.Supplier,
                                lokasi,
                                model.Total,
                                userid
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertReturMutasi tidak mendapatkan nobukti");

                            foreach (var x in model.Detail)
                            {
                                s.INV_Insert_BL_trPeminjamanReturDetail(
                                    id,
                                    model.NoPeminjaman,
                                    x.Barang,
                                    x.Qty,
                                    x.Harga,
                                    x.NamaBarang,
                                    x.KodeSatuan,
                                    lokasi,
                                    model.Tanggal);
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Mutasi-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_List_BL_trPeminjamanRetur_Header.FirstOrDefault(x => x.No_Retur == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_List_BL_trPeminjamanReturDetail.Where(x => x.No_Retur == m.No_Retur).OrderBy(x => x.NamaBarang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No_Retur = m.No_Retur,
                            //Tgl_Retur = m.Tgl_Retur.ToString("yyyy-MM-dd"),
                            No_Peminjaman = m.No_Peminjaman,
                            Tgl_Retur = m.Tgl_Retur == null ? "" : m.Tgl_Retur.Value.ToString("yyyy-MM-dd"),
                            Supplier_ID = m.Supplier_ID,
                            Nama_Supplier = m.Nama_Supplier,
                            Keterangan = m.Keterangan,
                            Lokasi_ID = m.Lokasi_ID
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.NamaBarang,
                            Satuan = x.Kode_Satuan,
                            Qty = x.Qty,
                            QtyRetur = x.Qty,
                            Harga = x.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupMutasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_List_BL_trPeminjaman_Header_LookupRetur.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Peminjaman.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "Tanggal" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Bonus == d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.No_Peminjaman,
                        Tanggal = m.Tgl_Bonus.Value.ToString("dd/MM/yyyy"),
                        Keterangan = m.Keterangan,
                        Nama_Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailPeminjaman(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.INV_List_BL_trPeminjamanDetail.Where(x => x.No_Peminjaman == id).OrderBy(x => x.NamaBarang).ToList();
                    var m = s.INV_List_BL_trPeminjaman_Header_LookupRetur.FirstOrDefault(x => x.No_Peminjaman == id);
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No_Peminjaman = m.No_Peminjaman,
                            Tanggal = m.Tgl_Bonus.Value.ToString("yyyy-MM-dd"),
                            SupplierID = m.Supplier_ID,
                            Lokasi = m.Lokasi_ID,
                            Nama_Supplier = m.Nama_Supplier,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.NamaBarang,
                            Satuan = x.Kode_Satuan,
                            Qty = x.QtySisa,
                            QtyRetur = x.Qty,
                            Harga = x.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}