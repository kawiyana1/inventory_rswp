﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PemakaianObatPasienController : Controller
    {
        private string tipepelayanan = "GUDANG";
        private string tipepelayanan2 = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan &&
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.Pelayanan_ListPemakaianObatNonPasien.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        No = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = $"{m.Jam.ToString("HH")}:{m.Jam.ToString("mm")}",
                        Keterangan = m.Keterangan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PemakaianObatPasienViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan &&
                Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var section = Request.Cookies["Inventory_Section_LokasiId"].Value;
                        int sectionid = int.Parse(section);
                        var userid = User.Identity.GetUserId();
                        var id = "";
                        if (_process == "CREATE")
                        {
                            id = s.AutoNumber_Pelayanan_GD_SIMtrPemakaian().FirstOrDefault();
                            var m = new SIMtrPemakaian();

                            m.NoBukti = id;
                            m.Tanggal = DateTime.Today;
                            m.UserID = 1103;
                            m.Jam = DateTime.Now;
                            m.SectionID = section;
                            m.Keterangan = model.Keterangan;
                            s.SIMtrPemakaian.Add(m);

                            s.SaveChanges();
                            #region Detail
                            foreach (var x in model.Detail)
                            {

                                x.NoBukti = m.NoBukti;
                                var getobtnonpasien = s.Pelayanan_GetObatNonPasien.FirstOrDefault(y => y.Barang_ID == x.BarangID && y.SectionID == section);
                                var n = new SIMtrPemakaianDetail()
                                {
                                    BarangID = getobtnonpasien.Barang_ID,
                                    Harga = getobtnonpasien.Harga_Jual ?? 0,
                                    Keterangan = null,
                                    NoBukti = m.NoBukti,
                                    Satuan = getobtnonpasien.Satuan_Stok,
                                    QtyStok = getobtnonpasien.Qty_Stok, // ????
                                    QtyPemakaian = x.QtyPemakaian
                                };
                                s.SIMtrPemakaianDetail.Add(n);
                                foreach (var xx in model.IsiFifo)
                                {
                                    s.IsiKartuGudangFIFO(
                                        sectionid,
                                        getobtnonpasien.Barang_ID,
                                        getobtnonpasien.Satuan_Stok,
                                        xx.QtyPemakaian,
                                        getobtnonpasien.Harga_Jual,
                                        m.NoBukti,
                                        564,
                                        0,
                                        xx.TglTransaksi.ToString("yyyy-MM-dd"),
                                        xx.ExpDate.ToString("yyyy-MM-dd"),
                                        0
                                    );
                                }
                                s.SaveChanges();

                            }
                            #endregion


                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"PemakianObatLuar-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.SIMtrPemakaian.FirstOrDefault(x => x.NoBukti == id);
                    var md = s.SIMtrPemakaianDetail.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            StatusBatal = m.StatusBatal,
                            JamBatal = m.JamBatal?.ToString("yyyy-MM-dd"),
                            TanggalBatal = m.TanggalBatal?.ToString("yyyy-MM-dd"),
                            AlasanBatal = m.AlasanBatal,
                        },
                        Detail = new
                        { 
                            
                        },
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}