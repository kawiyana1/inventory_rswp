﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class OtherSectionController : Controller
    {
        //private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.ADM_ListSection.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.SectionName.Contains(x.Value) ||
                                y.PenanggungJawab.Contains(x.Value)
                            );
                        else if (x.Key == "UnitBisnis" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.UnitBisnisID == x.Value);
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.SectionID,
                        Nama = x.SectionName,
                        x.PenanggungJawab,
                        x.TipePelayanan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherSectionModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            id = s.ADM_InsertSection(
                                model.Nama,
                                model.PenanggunJawab,
                                model.TipePelayanan,
                                model.RIOnly,
                                model.RuangBayi,
                                model.ICU,
                                model.Poliklinik,
                                model.KodeNoBukti,
                                model.UnitBisnis,
                                model.IPFarmasi,
                                model.Pelayanan,
                                model.BlokCoPayment,
                                model.Aktif,
                                model.KelompokSection,
                                model.JumlahTT,
                                model.Customer,
                                model.AkunMutasiIn,
                                model.AkunMutasiOut
                            ).FirstOrDefault();
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateSection(
                                model.Id,
                                model.Nama,
                                model.PenanggunJawab,
                                model.TipePelayanan,
                                model.RIOnly,
                                model.RuangBayi,
                                model.ICU,
                                model.Poliklinik,
                                model.KodeNoBukti,
                                model.UnitBisnis,
                                model.IPFarmasi,
                                model.Pelayanan,
                                model.BlokCoPayment,
                                model.Aktif,
                                model.KelompokSection,
                                model.JumlahTT,
                                model.Customer,
                                model.AkunMutasiIn,
                                model.AkunMutasiOut
                            );
                            s.SaveChanges();
                            id = model.Id;
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteSection(model.Id);
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherSection-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.ADM_GetSection.FirstOrDefault(x => x.SectionID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.SectionID,
                            Nama = m.SectionName,
                            PenanggunJawab = m.PenanggungJawab,
                            TipePelayanan = m.TipePelayanan,
                            KelompokSection = m.KelompokSection,
                            RIOnly = m.RIOnly,
                            RuangBayi = m.RBayi,
                            ICU = m.ICU,
                            Poliklinik = m.PoliKlinik,
                            KodeNoBukti = m.KodeNoBukti,
                            JumlahTT = m.JumlahTT,
                            UnitBisnis = m.UnitBisnisID,
                            IPFarmasi = m.IPFarmasi,
                            Pelayanan = m.Pelayanan,
                            BlokCoPayment = m.BlokCoPay,
                            Aktif = m.StatusAktif,
                            Customer = m.Customer_ID,
                            AkunMutasiIn = m.MutasiMasukAkun_ID,
                            AkunMutasiOut = m.MutasiKeluarAkun_ID,
                            CustomerNama = m.Nama_Customer,
                            AkunMutasiInNama = m.MutasiMasukAkun_Nama,
                            AkunMutasiOutNama = m.MutasiKeluarAkun_Nama,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P  D U P L I C A T E

        [HttpPost]
        public string SaveDuplicate(string _process, OtherSectionModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "EDIT")
                        {
                            id = s.ADM_InsertSection(
                                model.Nama,
                                model.PenanggunJawab,
                                model.TipePelayanan,
                                model.RIOnly,
                                model.RuangBayi,
                                model.ICU,
                                model.Poliklinik,
                                model.KodeNoBukti,
                                model.UnitBisnis,
                                model.IPFarmasi,
                                model.Pelayanan,
                                model.BlokCoPayment,
                                model.Aktif,
                                model.KelompokSection,
                                model.JumlahTT,
                                model.Customer,
                                model.AkunMutasiIn,
                                model.AkunMutasiOut
                            ).FirstOrDefault();
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherSection-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string DetailDuplicate(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.ADM_GetSection.FirstOrDefault(x => x.SectionID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.SectionID,
                            Nama = m.SectionName,
                            PenanggunJawab = m.PenanggungJawab,
                            TipePelayanan = m.TipePelayanan,
                            KelompokSection = m.KelompokSection,
                            RIOnly = m.RIOnly,
                            RuangBayi = m.RBayi,
                            ICU = m.ICU,
                            Poliklinik = m.PoliKlinik,
                            KodeNoBukti = m.KodeNoBukti,
                            JumlahTT = m.JumlahTT,
                            UnitBisnis = m.UnitBisnisID,
                            IPFarmasi = m.IPFarmasi,
                            Pelayanan = m.Pelayanan,
                            BlokCoPayment = m.BlokCoPay,
                            Aktif = m.StatusAktif,
                            Customer = m.Customer_ID,
                            AkunMutasiIn = m.MutasiMasukAkun_ID,
                            AkunMutasiOut = m.MutasiKeluarAkun_ID,
                            CustomerNama = m.Nama_Customer,
                            AkunMutasiInNama = m.MutasiMasukAkun_Nama,
                            AkunMutasiOutNama = m.MutasiKeluarAkun_Nama,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}