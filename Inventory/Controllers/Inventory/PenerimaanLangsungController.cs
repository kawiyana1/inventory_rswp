﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class PenerimaanLangsungController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            var model = new PenerimaanBarangLangsungViewModel();
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            using (var s = new SIMEntities())
            {
                model.PPNNilai = 11;
                var setup = s.SetupAwal.FirstOrDefault(x => x.SetupName == "PPNPersen");
                if (setup != null)
                {
                    model.PPNNilai = setup.Nilai.ToDecimal();
                }
            }
            ViewBag.PPNNilai = model.PPNNilai;
            return View(model);
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListPenerimaanBarangRealisasi.Where(x => x.GudangID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_DO.Contains(x.Value) ||
                                y.No_Penerimaan.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tanggal == null ? "" : m.Tanggal.ToString("dd/MM/yyyy"),
                        No = m.No_Penerimaan,
                        NoDO = m.No_DO,
                        //Nilai = m.Total_Nilai,
                        //Batal = m.Batal,
                        Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PenerimaanBarangLangsungViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                var section = Request.Cookies["Inventory_Section_Id"].Value;
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = 0;
                        if (_process == "CREATE")
                        {
                            if (section == "SEC081")
                            {
                                if (model.JenisBarang == "" || model.JenisBarang == null) throw new Exception("Jenis barang tidak boleh kosong");
                            }
                            if (model.Detail == null) model.Detail = new List<PenerimaanBarangLangsungDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertPenerimaanBarang(
                                model.Tanggal,
                                model.Potongan,
                                model.DP,
                                model.Remak,
                                model.OngkosKirim,
                                model.NoDO,
                                model.SupplierId,
                                model.OrderId,
                                model.Total,
                                model.LokasiId,
                                model.JatuhTempo,
                                model.PPN,
                                model.AutoDO,
                                model.PPNNilai,
                                model.NoPenerimaanManual,
                                userid,
                                model.JenisBarang
                            ).FirstOrDefault() ?? 0;

                            if (id == 0) throw new Exception("INV_InsertPermintaan tidak mendapatkan nobukti");
                            foreach (var x in model.Detail)
                            {
                                //if (x.TanggalExp == null) throw new Exception("Tanggal ED Obat Tidak Boleh Kosong");
                                if (x.Qty == 0) throw new Exception("Qty Tidak Boleh Kosong");
                                var jum = (decimal)x.Qty * x.Harga;
                                var diskon = jum / 100 * (decimal)x.Disc;

                                s.INV_InsertPenerimaanBarangDetail(
                                    x.Harga,
                                    x.Disc,
                                    x.Qty,
                                    x.TanggalExp,
                                    (int)id,
                                    x.Id,
                                    diskon,
                                    x.NoBatch,
                                    model.OrderId,
                                    model.LokasiId,
                                    x.NIE
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.INV_InsertPenerimaanBarangDelete((int)model.Id);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            int lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListPenerimaanBarangHeader.FirstOrDefault(x => x.No_Penerimaan == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListPenerimaanBarangDetail.Where(x => x.Penerimaan_ID == m.Penerimaan_ID).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            SupplierId = m.Supplier_ID,
                            OrderId = m.Order_ID,
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tgl_Penerimaan.ToString("yyyy-MM-dd"),
                            NoPo = m.NoPO,
                            TanggalOrder = m.Tgl_Order == null ? "" : m.Tgl_Order.Value.ToString("yyyy-MM-dd"),
                            SupplierNama = m.Nama_Supplier,
                            Lokasi = m.Lokasi_ID,
                            NoDO = m.No_DO,
                            JatuhTempo = m.Tgl_JatuhTempo,
                            Remak = m.Keterangan,
                            NoPenerimaanManual = m.NoPenerimaanManual,
                            SubTotal = m.Total_Nilai,
                            Id = m.Penerimaan_ID
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Nama_Satuan,
                            QtyPO = x.Qty_PO,
                            TlhTerima = x.Qty_Telah_Terima,
                            QtyTerima = x.Qty_Penerimaan,
                            Harga = x.Harga,
                            Disc = x.Disc,
                            TanggalExp = x.Exp_Date == null ? "" : x.Exp_Date.Value.ToString("yyyy-MM-dd"),
                            NoBatch = x.NoBatch == null ? "" : x.NoBatch
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_LookupBarangBebas.Where(x => x.Lokasi_ID == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kategori.Contains(x.Value));
                        else if (x.Key == "Lokasi")
                        {
                            var _v = int.Parse(x.Value);
                            proses = proses.Where(y => y.Lokasi_ID == _v);
                        }
                        if (x.Key == "SubKategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _x = int.Parse(x.Value);
                            proses = proses.Where(y =>
                            y.SubKategori_Id == _x
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                        Stok = x.Qty_Stok,
                        Kategori = x.Kategori,
                        SubKategori_Id = x.SubKategori_Id,
                        Nama_Sub_Kategori = x.Nama_Sub_Kategori,
                        MinStok = x.Min_Stok,
                        MaxStok = x.Max_Stok,
                        QtyStok = x.Qty_Stok,
                        Harga = x.Harga_Beli
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}