﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class ReturMutasiController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangReturMutasi.Where(x => x.Lokasi_Asal == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBuktiMutasi.Contains(x.Value) ||
                                y.No_Bukti.Contains(x.Value) ||
                                y.ReturTujuan_Nama_Lokasi.Contains(x.Value) ||
                                y.ReturAsal_Nama_Lokasi.Contains(x.Value));
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi <= d);
                        }
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.No_Bukti,
                        NoMutasi = m.NoBuktiMutasi,
                        TglMutasi = m.Tgl_Mutasi.ToString("dd/MM/yyyy"),
                        AsalRetur = m.ReturAsal_Nama_Lokasi,
                        ReturTujuan = m.ReturTujuan_Nama_Lokasi,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, ReturMutasiViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (string.IsNullOrEmpty(model.NoMutasi)) throw new Exception("Mutasi Harus diisi");
                            if (model.Detail == null) model.Detail = new List<ReturMutasiDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            foreach (var x in model.Detail)
                            {
                                if (x.Qty <= 0) throw new Exception("Qty tidak boleh kurang dari sama dengan 0");
                            }
                            id = s.INV_InsertReturMutasi(
                                model.NoMutasi,
                                model.Tanggal,
                                model.Keterangan
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertReturMutasi tidak mendapatkan nobukti");

                            foreach (var x in model.Detail)
                            {
                                s.INV_InsertReturMutasiDetail(id, x.Qty, x.Id);
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Mutasi-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangReturMutasiHeader.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListGudangReturMutasiDetail.Where(x => x.No_Bukti == m.No_Bukti).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.No_Bukti,
                            Tanggal = m.Tgl_ReturMutasi.ToString("yyyy-MM-dd"),
                            NoMutasi = m.NoMutasi,
                            TanggalMutasi = m.Tgl_Mutasi == null ? "" : m.Tgl_Mutasi.Value.ToString("yyyy-MM-dd"),
                            AsalRetur = m.LokasiAsal,
                            TujuanRetur = m.TujuanReturNama,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Konversi = x.Konversi,
                            Satuan = x.NamaSatuan,
                            Kategori = x.Nama_Kategori,
                            Stok = x.Qty_Stok,
                            Amprahan = x.QtyAmprah,
                            Qty = x.Qty,
                            Harga = x.Harga,
                            KodeAkun = x.KodeAkun,
                            NamaAkunMutasi = x.NamaAkun
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupMutasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangMutasiHeader.Where(x => x.Lokasi_Asal == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value)) 
                        {
                            proses = proses.Where(y => 
                                y.No_Bukti.Contains(x.Value) ||
                                y.SectionTujuan.Contains(x.Value) ||
                                y.SectionAsal.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.TglAmprahan >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.TglAmprahan <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.No_Bukti,
                        Tanggal = m.Tgl_Mutasi.ToString("dd/MM/yyyy"),
                        SectionAsal = m.SectionAsal,
                        SectionTujuan = m.SectionTujuan,
                        Keterangan = m.Keterangan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string DetailMutasi(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangMutasiHeader.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListGudangMutasiDetail.Where(x => x.No_Bukti == m.No_Bukti).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.No_Bukti,
                            Tanggal = m.Tgl_Mutasi.ToString("yyyy-MM-dd"),
                            SectionAsal = m.SectionAsal,
                            SectionTujuan = m.SectionTujuan,
                        },
                        Detail = d.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Konversi = x.Konversi,
                            Satuan = x.Satuan,
                            Kategori = x.Nama_Kategori,
                            Stok = x.Qty_Stok,
                            Amprahan = x.QtyAmprah,
                            Qty = x.Qty,
                            Harga = x.Harga,
                            KodeAkun = x.KodeAkun,
                            NamaAkunMutasi = x.AkunName
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}