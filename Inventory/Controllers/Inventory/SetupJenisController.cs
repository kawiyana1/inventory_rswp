﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class SetupJenisController : Controller
    {
        private string tipepelayanan = "GUDANG";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.INV_mJenis.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => 
                                y.KelompokJenis.Contains(x.Value) ||
                                y.Kelompok.Contains(x.Value) ||
                                y.Akun_Name_Pembelian.Contains(x.Value) ||
                                y.KelompokPR.Contains(x.Value) ||
                                y.Akun_Name_Mutasi.Contains(x.Value)
                            );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Nama = x.KelompokJenis,
                        Kelompok = x.Kelompok,
                        AkunPembelian = x.Akun_Name_Pembelian,
                        Jenis = x.KelompokPR,
                        AkunMutasi = x.Akun_Name_Mutasi,
                        MutasiTidakDiPosting = x.TidakPostingMutasi
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupJenisModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.INV_InsertJenisBarang(model.Nama, model.Kelompok, model.AkunPembelian, model.AkunMutasi, model.Jenis, model.MutasiTidakDiPosting);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.INV_UpdateJenisBarang(model.Nama, model.Kelompok, model.AkunPembelian, model.AkunMutasi, model.Jenis, model.MutasiTidakDiPosting);
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupJenis-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.INV_mJenis.FirstOrDefault(x => x.KelompokJenis == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Nama = m.KelompokJenis,
                            Kelompok = m.Kelompok,
                            AkunPembelian = m.Akun_ID_Pembelian,
                            Jenis = m.KelompokPR_ID,
                            AkunMutasi = m.Akun_ID_Mutasi,
                            MutasiTidakDiPosting = m.TidakPostingMutasi
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}