﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    public class FarmasiInfoHargaObatController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                var section = Request.Cookies["Inventory_Section_Id"].Value;

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var barang = filter.FirstOrDefault(x => x.Key == "Barang")?.Value;
                    var tipepasien = filter.FirstOrDefault(x => x.Key == "TipePasien")?.Value;
                    var proses = s.FAR_InfoHargaObat(
                            tipepasien == null ? (int?)null : int.Parse(tipepasien),
                            barang == null ? (int?)null : int.Parse(barang), 
                            section).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.KodeBarang.Contains(x.Value)
                            );
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.KodeBarang,
                        Nama = m.Nama_Barang,
                        Satuan = "", // ---------------------------
                        Stok = m.Stok,
                        Harga = (m.Harga ?? 0).ToMoney()
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}