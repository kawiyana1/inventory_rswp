﻿using Inventory.Entities.SIM;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            
            try
            {
                using (var s = new SIMEntities())
                {
                    var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));

                    if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                    var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                    if (Request.Cookies["Inventory_Section_Group"].Value == "GUDANG")
                    {
                        model.Inventory_PermintaanPembelian = s.INV_ListPermintaanPembelian.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.Gudang_ID == lokasi).Count();
                        model.Inventory_PenerimaanBarang = s.INV_ListPenerimaanBarangRealisasi.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.GudangID == lokasi).Count();
                        model.Inventory_Amprahan = s.INV_ListGudangAmprahan.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.LokasiTujuan == lokasi).Count();
                        model.Inventory_MutasiBarang = s.INV_ListGudangReturMutasi.Where(x => x.Tgl_Mutasi >= start && x.Tgl_Mutasi <= end && x.Lokasi_Tujuan == (short)lokasi).Count();
                    }
                    else if (Request.Cookies["Inventory_Section_Group"].Value == "PENGADAAN")
                    {
                        model.Purchasing_Open = s.INV_listPengadaaanOrder.Where(x => x.Tgl_Permintaan >= start && x.Tgl_Permintaan <= end).Count();
                        model.Purchasing_BelumTerima = s.INV_listPengadaaanOrderRealisasi.Where(x => x.Tgl_Order >= start && x.Tgl_Order <= end && x.Status == "Belum Terima").Count();
                        model.Purchasing_BelumSemua = s.INV_listPengadaaanOrderRealisasi.Where(x => x.Tgl_Order >= start && x.Tgl_Order <= end && x.Status == "Terima Sebagian").Count();
                        model.Purchasing_SudahTerima = s.INV_listPengadaaanOrderRealisasi.Where(x => x.Tgl_Order >= start && x.Tgl_Order <= end && x.Status == "Terima Semua").Count();
                    }
                    else if (Request.Cookies["Inventory_Section_Group"].Value == "FARMASI")
                    {
                        if (Request.Cookies["Inventory_Section_Id"] == null) throw new Exception("Section Id tidak ditemukan");
                        var section = Request.Cookies["Inventory_Section_Id"].Value;

                        model.Farmasi_Resep = s.FAR_ListResepAll.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.Farmasi_SectionID == section).Count();
                        model.Farmasi_Billing = s.FAR_ListBilling.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.Farmasi_SectionID == section).Count();
                        model.Farmasi_BillingObatBebas = s.FAR_ListBillingObatBebas.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.Farmasi_SectionID == section).Count();
                        model.Farmasi_PembelianObatLuar = s.INV_ListPembelianObatLuar.Where(x => x.Tanggal >= start && x.Tanggal <= end && x.Lokasi_ID == (short)lokasi).Count();
                    }
                    return View(model);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult ApplicationInfo()
        {
            return View();
        }

        public ActionResult Help()
        {
            return View();
        }
    }
}