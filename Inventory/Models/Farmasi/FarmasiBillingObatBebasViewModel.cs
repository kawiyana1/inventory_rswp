﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiBillingObatBebasViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public decimal SubTotal { get; set; }
        public decimal BiayaRacik { get; set; }
        public string Dokter { get; set; }
        public int TipePasien { get; set; }
        public string Perusahaan { get; set; }
        public string NoKartu { get; set; }
        public string Nama { get; set; }
        public bool  Karyawan { get; set; }
        public string  CaraBayar { get; set; }
        public string  NoReg { get; set; }
        public List<FarmasiBillingObatBebasDetailViewModel> Detail { get; set; }
    }

    public class FarmasiBillingObatBebasDetailViewModel 
    {
        public int Id { get; set; }
        public double JumlahObat { get; set; }
        public double QtyStok { get; set; }
        public bool Lock { get; set; }
        public string Aturan1 { get; set; }
        public string Aturan2 { get; set; }
        [Required]
        public DateTime? TanggalED { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
        public string Racik { get; set; }
        public bool WaktuTiket1 { get; set; }
        public bool WaktuTiket2 { get; set; }
        public bool WaktuTiket3 { get; set; }
        public bool WaktuTiket4 { get; set; }
        public bool WaktuTiket5 { get; set; }
        public bool WaktuTiket6 { get; set; }
        public bool WaktuTiket7 { get; set; }
        public bool WaktuTiket8 { get; set; }
        public bool WaktuTiket9 { get; set; }
        public bool WaktuTiket10 { get; set; }
        public bool WaktuTiket11 { get; set; }
        public bool WaktuTiket12 { get; set; }
    }
}