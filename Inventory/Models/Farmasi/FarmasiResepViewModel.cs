﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiResepViewModel
    {
        public DateTime Tanggal { get; set; }
        public DateTime TanggalResep { get; set; }
        public bool Paket { get; set; }
        public string NoReg { get; set; }
        public string KodePaket { get; set; }
        public decimal BiayaRacik { get; set; }
        public decimal Total { get; set; }
        //public string Dokter { get; set; }
        //public string SectionAsal { get; set; }
        public string NoResep { get; set; }
        //public string NoReg { get; set; }
        public decimal KelebihanPlafon { get; set; }
        public string Keterangan { get; set; }
        public bool IncludeJasa { get; set; }
        public List<FarmasiResepDetailViewModel> Detail { get; set; }
    }

    public class FarmasiResepDetailViewModel
    {
        public int Id { get; set; }
        public double Obat { get; set; }
        public double JumlahObat { get; set; }
        public double QtyStok { get; set; }
        public bool Lock { get; set; }
        public string AturanPakai1 { get; set; }
        public string AturanPakai2 { get; set; }
        public DateTime? TanggalED { get; set; }
        public decimal Harga { get; set; }
        public double Disc { get; set; }
        public string Racik { get; set; }
        public bool WaktuTiket1 { get; set; }
        public bool WaktuTiket2 { get; set; }
        public bool WaktuTiket3 { get; set; }
        public bool WaktuTiket4 { get; set; }
        public bool WaktuTiket5 { get; set; }
        public bool WaktuTiket6 { get; set; }
        public bool WaktuTiket7 { get; set; }
        public bool WaktuTiket8 { get; set; }
        public bool WaktuTiket9 { get; set; }
        public bool WaktuTiket10 { get; set; }
        public bool WaktuTiket11 { get; set; }
        public bool WaktuTiket12 { get; set; }
    }

    public class detailTelaahFarmasi{
        public int NoUrut { get; set; }
        public bool cekTelaah { get; set; }
        public string ketTelaah { get; set; }
    }

    public class detailTelaahObat
    {
        public int NoUrut { get; set; }
        public bool cekTelaah { get; set; }
        public string ketTelaah { get; set; }
    }
}