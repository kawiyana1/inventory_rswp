﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class HomeViewModel
    {
        public int Inventory_PermintaanPembelian { get; set; }
        public int Inventory_PenerimaanBarang { get; set; }
        public int Inventory_Amprahan { get; set; }
        public int Inventory_MutasiBarang { get; set; }

        public int Purchasing_Open { get; set; }
        public int Purchasing_BelumTerima { get; set; }
        public int Purchasing_BelumSemua { get; set; }
        public int Purchasing_SudahTerima { get; set; }

        public int Farmasi_Resep { get; set; }
        public int Farmasi_Billing { get; set; }
        public int Farmasi_BillingObatBebas { get; set; }
        public int Farmasi_PembelianObatLuar { get; set; }
    }
}