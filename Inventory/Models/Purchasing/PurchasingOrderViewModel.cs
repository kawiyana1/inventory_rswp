﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Purchasing
{
    public class PurchasingOrderViewModel
    {
        public DateTime Tanggal { get; set; }
        public string Keterangan { get; set; }
        public int Supplier { get; set; }
        public decimal Total { get; set; }
        public string NoPermintaan { get; set; }
        public int JenisPengadaan { get; set; }
        public List<PurchasingDetailOrderViewModel> Detail { get; set; }
    }

    public class PurchasingDetailOrderViewModel
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public decimal Harga { get; set; }
    }
}