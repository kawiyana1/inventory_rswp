﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class StokOpnameViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public int Lokasi { get; set; }
        public int SubKategori { get; set; }
        public string KelompokJenis { get; set; }
        public string Inisial { get; set; }
        public string KodeKelompok { get; set; }
        public string KeteranganSO { get; set; }
        public List<StokOpnameDetailViewModel> Detail { get; set; }

        public string Setup { get; set; }
    }

    public class StokOpnameDetailViewModel 
    {
        public int Id { get; set; }
        public double Qty { get; set; }
        public string Keterangan { get; set; }
    }

}