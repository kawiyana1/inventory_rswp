﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class SetupJenisModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public string Kelompok { get; set; }
        public int AkunPembelian { get; set; }
        public int Jenis { get; set; }
        public int AkunMutasi { get; set; }
        public bool MutasiTidakDiPosting { get; set; }
    }
}