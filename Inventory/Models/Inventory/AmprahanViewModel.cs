﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class AmprahanPRViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public int Gudang { get; set; }
        public int KelompokPR { get; set; }
        public string Keterangan { get; set; }
        public DateTime TanggalDibutuhkan { get; set; }
        public decimal Total { get; set; }
        public int SupplierID { get; set; }
        public List<AmprahanPRDetailViewModel> Detail { get; set; }
    }
     
    public class AmprahanViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public string DariSection { get; set; }
        public string Kepada { get; set; }
        public string Keterangan { get; set; }
        public List<AmprahanDetailViewModel> Detail { get; set; }
    }

    public class AmprahanDetailViewModel 
    {
        public int Id { get; set; } 
        public double Qty { get; set; }
    }

    public class AmprahanPRDetailViewModel
    {
        public int Id { get; set; }
        public decimal Harga { get; set; }
        public double Qty { get; set; }
    }
}