﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class SetupKelasBarangModel
    {
        public int Id { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int SubKategori { get; set; }
    }
}