﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class MutasiViewModel
    {
        public DateTime Tanggal { get; set; }
        public int MutasiDari { get; set; }
        public string NoAmprahan { get; set; }
        public string Keterangan { get; set; }
        public List<MutasiDetailViewModel> Detail { get; set; }
    }

    public class MutasiDetailViewModel 
    {
        public int Id { get; set; }
        public double Qty { get; set; }
        public double QtyStok { get; set; }
        public string Lock { get; set; }
    }
}